Шаблон проекта по ИСиТ
======================

Перед использованием шаблона нужно установить:
----------------------------------------------
  * [CMake][cmake],
  * [Git][git] (в Windows разрешите интеграцию с *Проводником*),
  * компилятор и IDE, например, [как в лаборатории][tools].

Настройки по умолчанию годятся.

Чтобы начать новый проект, нужно:
---------------------------------
  1. На странице репозитария в BitBucket нажать **...** (слева) и выбрать *Fork*.

  2. Ввести имя, под которым будет создан проект у вашего пользователя
     и нажать *Fork repository*.

  3. На своем компьютере клонировать *свой* репозитарий по адресу, написанному
     справа вверху в варианте HTTPS:

     ![](address.png)
     
     В терминале это можно сделать командой *наподобие*:

    `git clone https://пользователь@bitbucket.org/пользователь/репозитарий.git`

     В Windows для открытия терминала используйте пункт *Git Bash here*
     в контекстном меню папки, куда должен быть помещен репозитарий.
     Этот терминал подобен тому, что в Linux.

  4. Выполнить *в каталоге репозитария* (например, `inat-lab-test`) команды:

     `git submodule init`

     `git submodule update --remote`

  5. Отредактировать CMakeLists.txt по комментариям в нем.

  6. Сгенерировать проект CodeBlocks или CodeLite, запустив соответственно
     `setup-codeblocks.sh` или `setup-codelite.sh` (в Linux, в Windows ---
     те же файлы, но с расширением `*.bat`) двойным щелчком.

     В репозитарии появится файл проекта (`*.cbp` для CodeBlocks
     или `*.workspace` для CodeLite), которые можно открыть и работать далее
     в IDE.

  7. В CodeBlocks потребуется в диалоге *Build options...*, вызываемом
     из контекстного меню проекта, выбрать компилятор *MinGW-w64 GCC*.

Сохранить свои наработки на сервере можно так:
----------------------------------------------
  1. Ввести команду `git status`, чтобы просмотреть состояние файлов.
  
  2. Командой `git add <файл>` добавить все нужный файлы в индекс (перечень
     изменений для включения в коммит).  Можно указывать несколько файлов через
     пробел. Все измененные (но не новые) файлы можно добавить `git add -u`.

  3. Создать коммит командой `git commit`, вписать сообщение.

  4. Отправить изменения на сервер командой `git push`.

Загрузить изменения с сервера можно так:
----------------------------------------
  * Если на данном компьютере проекта еще не было, повторить пункты 3 и 4
    инструкции по созданию проекта.
  
  * Иначе, если проект на данном компьютере уже есть, выполнить в репозитарии
    команду `git pull`.

[cmake]: https://cmake.org/download/
[git]: https://git-scm.com/downloads
[tools]: http://uii.mpei.ru/study/courses/common/ide.html
